import {LoginComponent} from "./login/login.component";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {AuthenticationService} from "./authentication.service";
import {AuthGuard} from "../guards/auth.guard";
import {RoleGuard} from "../guards/role.guards";

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    LoginComponent
  ],
  exports: [
    LoginComponent
  ],
  providers: [
    AuthenticationService,
    AuthGuard,
    RoleGuard
  ]

})
export class LoginModule {
}
