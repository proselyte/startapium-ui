import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {AuthenticationService} from "../authentication.service";


@Component({
  moduleId: module.id,
  templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit {
  user: any = {};
  loading = false;
  returnUrl: string;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private authenticationService: AuthenticationService) {
  }

  ngOnInit() {

    this.authenticationService.logout();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  goToRegister() {

    this.router.navigate(['/app/register']);
  }

  login(value) {
    this.loading = true;
    this.authenticationService.login(value)
      .subscribe(
        data => {
          // this.router.navigate([this.returnUrl]);
          this.router.navigate(['/app/welcome']);
        },
        error => {
          this.router.navigate(['/app/login']);
          this.loading = false;
        });
  }

}
