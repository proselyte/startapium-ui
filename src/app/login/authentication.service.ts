import {Injectable} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import 'rxjs/add/operator/map'
import {AuthGuard} from "../guards/auth.guard";
import {User} from "../user/user";
import {Observable} from "rxjs/Observable";
import {environment} from "../../environments/environment";
import {RoleGuard} from "../guards/role.guards";

@Injectable()
export class AuthenticationService {

  private entity_url = environment.REST_API_URL;

  constructor(private http: Http,
              private authGuard: AuthGuard,
              private roleGuard: RoleGuard) {
  }

  login(value: any) {
    return this.http.
    post(this.entity_url + 'app/auth/login',value)
      .map(res => {
        const data = res.json();
        if (data) {
          localStorage.setItem('token', data.token);
          localStorage.setItem('user', JSON.stringify(data.user));
          this.authGuard.isAuthenticated = true;
          const user : User = JSON.parse(localStorage.getItem('user'));
          this.authGuard.userName = user.name;
          user.roles.forEach(roleUser => {
            if (roleUser.name === 'ROLE_ADMIN') {
              this.roleGuard.isAdmin = true;
            }
          });
        }
      });
  }


  getRoles() {
    const user: User = JSON.parse(localStorage.getItem('user'));
    return user.roles;
  }

  logout() {
    this.http.post(this.entity_url + 'app/auth/logout', {});
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    this.authGuard.isAuthenticated = false;
    this.roleGuard.isAdmin = false;
    this.authGuard.userName = null;
  }

  private handleError(error: Response | any) {
    console.log('handleError log: ');
    let errMsg: string;
    if (error instanceof Response) {
      if (!(error.text() === '' )) {
        const body = error.json() || '';
        const err = body.error || JSON.stringify(body);
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      } else {
        console.log('binding errors header not empty');
        errMsg = error.headers.get('errors').toString();
      }
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
