import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from "../../user/user.service";


@Component({
  moduleId: module.id,
  templateUrl: './register.component.html'
})

export class RegisterComponent {
  user: any = {};
  loading = false;
  types = ['customer', 'specialist'];

  constructor(private router: Router,
              private userService: UserService) {
  }

  register() {
    this.loading = true;
    this.userService.signUp(this.user)
      .subscribe(
        data => {
          this.goToLogin();
        },
        error => {
          this.loading = false;
        });
  }

  goToLogin() {

    this.router.navigate(['/app/login']);
  }
}
