import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppComponent} from './app.component';
import {AppRoutingModule} from "./app-routing.module";
import {AuthGuard} from "./guards/auth.guard";
import {RoleGuard} from "./guards/role.guards";
import {AdminInputModule} from "./admin-input/admin-input.module";
import {AppInputModule} from "./app-input/app-input.module";
import {PartsModule} from "./parts/parts.module";
import {UserModule} from "./user/admin/user.module";
import {RoleModule} from "./role/role.module";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    PartsModule,
    UserModule,
    RoleModule,
    AppInputModule,
    AdminInputModule,
    AppRoutingModule,
  ],
  providers: [AuthGuard, RoleGuard],
  bootstrap: [AppComponent]
})
export class AppModule {
}
