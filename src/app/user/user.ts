
import {Role} from "../role/role";


export class User {

  private _id: string;
  private _name: string;
  private _firstName: string;
  private _lastName: string;
  private _password: string;
  private _email: string;
  private _userType: string;
  private _birthDate: string;
  private _avatar_id: string;
  private _roles:Role[];

  constructor(){

  }
//
//   constructor(id: string, name: string,
//               firstName: string, lastName: string,
//               password: string, email: string,
//               userType: string, birthDate: string,
//               avatar_id: string, roles: Role[]) {
// this();
//     this._id = id;
//     this._name = name;
//     this._firstName = firstName;
//     this._lastName = lastName;
//     this._password = password;
//     this._email = email;
//     this._userType = userType;
//     this._birthDate = birthDate;
//     this._avatar_id = avatar_id;
//     this._roles = roles;
//   }

  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get firstName(): string {
    return this._firstName;
  }

  set firstName(value: string) {
    this._firstName = value;
  }

  get lastName(): string {
    return this._lastName;
  }

  set lastName(value: string) {
    this._lastName = value;
  }

  get password(): string {
    return this._password;
  }

  set password(value: string) {
    this._password = value;
  }

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  get userType(): string {
    return this._userType;
  }

  set userType(value: string) {
    this._userType = value;
  }

  get birthDate(): string {
    return this._birthDate;
  }

  set birthDate(value: string) {
    this._birthDate = value;
  }

  get roles(): Role[] {
    return this._roles;
  }

  set roles(value: Role[]) {
    this._roles = value;
  }

  get avatar_id(): string {
    return this._avatar_id;
  }

  set avatar_id(value: string) {
    this._avatar_id = value;
  }
}
