import {Component, OnInit} from '@angular/core';
import {UserService} from "../../user.service";
import {ActivatedRoute, Router} from "@angular/router";
import {RoleGuard} from "../../../guards/role.guards";

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html'
})
export class UserAddComponent implements OnInit {

  private _errorMessage: string;
  private _user: any = {};
  private _rGuard: RoleGuard;
  private _isDuplicateLogin: boolean = true;
  private _returnUrl: string;
  private _types = ['customer', 'specialist'];

  constructor(private roleGuard: RoleGuard,
              private route: ActivatedRoute,
              private router: Router,
              private userService: UserService) {
    this._rGuard = roleGuard;
  }

  ngOnInit(): void {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  addUser() {

    this.userService.isEmptyName(this.user.name)
      .subscribe(
        isEmpty => {
          if (isEmpty) {
            this.isDuplicateLogin = true;
            this.userService.save(this.user)
              .subscribe(
                user => {
                  this.user = user;
                  this.goToUserPage();
                },
                error => {
                  this.errorMessage = <any> error;
                });
          }else {
            this.isDuplicateLogin = false;
          }
        },
        error => {
          this.errorMessage = <any> error;
        });
  }


  goToUserPage() {
    this.router.navigate(['/admin/user/', this._user.id]);
  }

  returnToBack() {
    window.history.back();
  }


  get errorMessage(): string {
    return this._errorMessage;
  }

  set errorMessage(value: string) {
    this._errorMessage = value;
  }

  get user(): any {
    return this._user;
  }

  set user(value: any) {
    this._user = value;
  }

  get rGuard(): RoleGuard {
    return this._rGuard;
  }

  set rGuard(value: RoleGuard) {
    this._rGuard = value;
  }

  get isDuplicateLogin(): boolean {
    return this._isDuplicateLogin;
  }

  set isDuplicateLogin(value: boolean) {
    this._isDuplicateLogin = value;
  }

  get returnUrl(): string {
    return this._returnUrl;
  }

  set returnUrl(value: string) {
    this._returnUrl = value;
  }

  get types(): (string | string)[] {
    return this._types;
  }

  set types(value: (string | string)[]) {
    this._types = value;
  }

}
