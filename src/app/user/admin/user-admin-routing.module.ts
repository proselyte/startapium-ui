import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {AdminInputComponent} from "../../admin-input/admin-input/admin-input.component";
import {RoleGuard} from "../../guards/role.guards";
import {UserAddComponent} from "./user-add/user-add.component";
import {UserInfoComponent} from "./user-info/user-info.component";
import {UsersListComponent} from "./users-list/users-list.component";
import {UserDeleteComponent} from "./user-delete/user-delete.component";
import {UserAdminComponent} from "./user.admin.component";
import {UserEditComponent} from "./user-edit/user-edit.component";


const userAdminRoutes: Routes = [
  {
    path: 'admin',
    component: AdminInputComponent,
    children: [
      {
        path: 'user',
        component: UserAdminComponent,
        children: [
          {
            path: 'all',
            component: UsersListComponent
          },
          {
            path: 'add',
            component: UserAddComponent
          },
          {
            path: 'edit',
            children: [
              {
                path: ':id',
                component: UserEditComponent
              },
            ],
          },
          {
            path: 'delete',
            children: [
              {
                path: ':id',
                component: UserDeleteComponent
              },
            ],
          },
          {
            path: ':id',
            component: UserInfoComponent
          },
        ],
        canActivate: [RoleGuard]

      }]
  }
];


@NgModule({
  imports: [RouterModule.forChild(userAdminRoutes)],
  exports: [RouterModule]
})

export class UserAdminRoutingModule {
}
