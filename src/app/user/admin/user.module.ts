import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserAdminRoutingModule} from "./user-admin-routing.module";
import {RoleGuard} from "../../guards/role.guards";
import {AuthGuard} from "../../guards/auth.guard";
import {UserService} from "../user.service";
import {FormsModule} from "@angular/forms";
import {UsersListComponent} from "./users-list/users-list.component";
import {UserInfoComponent} from "./user-info/user-info.component";
import {UserEditComponent} from "./user-edit/user-edit.component";
import {UserDeleteComponent} from "./user-delete/user-delete.component";
import {UserAddComponent} from "./user-add/user-add.component";
import {UserAdminComponent} from "./user.admin.component";
import {BootstrapModalModule} from "angular2-modal/plugins/bootstrap";
import {ModalModule} from "angular2-modal";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ModalModule.forRoot(),
    BootstrapModalModule,
    UserAdminRoutingModule
  ],
  declarations: [
    UserInfoComponent,
    UserEditComponent,
    UserDeleteComponent,
    UserAddComponent,
    UsersListComponent,
    UserAdminComponent
  ],
  providers: [
    UserService,
    AuthGuard,
    RoleGuard]
})
export class UserModule {
}
