import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {UserService} from "../../user.service";
import {ActivatedRoute, Router} from "@angular/router";
import {RoleGuard} from "../../../guards/role.guards";
import {User} from "../../user";
import {Modal} from 'angular2-modal/plugins/bootstrap';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html'
})
export class UserInfoComponent implements OnInit {
  private _user: User = new User();

  private _errorMessage: string;
  private _returnUrl: string;
  private _rGuard: RoleGuard;

  constructor(vcRef: ViewContainerRef,
              public modal: Modal,
              private roleGuard: RoleGuard,
              private route: ActivatedRoute,
              private router: Router,
              private userService: UserService) {
    this._rGuard = roleGuard;
    this.modal.overlay.defaultViewContainer = vcRef;

    const id = this.route.snapshot.params['id'];
    this.userService.getById(id)
      .subscribe(
        user => this._user = user,
        error => this._errorMessage = <any> error)
    ;

  }

  ngOnInit() {
    this._returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  openModal() {
    this.modal.confirm()
      .size('sm')
      .showClose(true)
      .title('WARNING')
      .body(`<h4>are you sure?</h4>`)
      .okBtn('yes')
      .okBtnClass('btn btn-warning')
      .dialogClass('')
      .open()
      .then((resultPromise) => {
        resultPromise.result.then((result) => {
          this.deleteUser();
        });
      });
  }

  deleteUser() {
    this.userService.delete(this._user.id)
      .subscribe(
        user => {
          this._user = user;
          this.goToUserList();
        },
        error => {
          this._errorMessage = <any> error;
        });
  }

  goToUserList() {
    this.router.navigate(['/admin/user/all']);
  }

  returnToBack() {
    window.history.back();
  }

  goToEdit() {
    this.router.navigate(['/admin/user/edit/', this._user.id]);
  }

  get user(): User {
    return this._user;
  }

  set user(value: User) {
    this._user = value;
  }

  get errorMessage(): string {
    return this._errorMessage;
  }

  set errorMessage(value: string) {
    this._errorMessage = value;
  }

  get returnUrl(): string {
    return this._returnUrl;
  }

  set returnUrl(value: string) {
    this._returnUrl = value;
  }

  get rGuard(): RoleGuard {
    return this._rGuard;
  }

  set rGuard(value: RoleGuard) {
    this._rGuard = value;
  }
}
