import {Component} from "@angular/core";


@Component({
  selector: 'user-admin',
  template: '<router-outlet></router-outlet>'
})
export class UserAdminComponent {

  constructor() {
  }


}
