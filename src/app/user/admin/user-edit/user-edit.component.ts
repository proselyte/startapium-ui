import {Component, OnInit} from '@angular/core';
import {UserService} from "../../user.service";
import {ActivatedRoute, Router} from "@angular/router";
import {RoleGuard} from "../../../guards/role.guards";

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html'
})
export class UserEditComponent implements OnInit {
  private _errorMessage: string;
  private _user: any = {};
  private _login: string;
  private _rGuard: RoleGuard;
  private _returnUrl: string;
  private _isDuplicateLogin: boolean = true;
  private _types = ['customer', 'specialist'];

  constructor(private roleGuard: RoleGuard,
              private route: ActivatedRoute,
              private router: Router,
              private userService: UserService) {
    this._rGuard = roleGuard;
  }

  ngOnInit(): void {
    this._returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

    const id = this.route.snapshot.params['id'];
    this.userService.getById(id).subscribe(
      user => {
        this._user = user;
        this._login = user.name;
      },
      error => {
        this._errorMessage = <any> error
      }
    );
  }

  editUser() {
    if (this._login === this._user.name) {
      this.isDuplicateLogin = true;
      this.userService.update(this.user)
        .subscribe(
          user => {
            this.user = user;
            this.goToUserPage();
          },
          error => {
            this.errorMessage = <any> error;
          });
    } else {
      this.userService.isEmptyName(this.user.name)
        .subscribe(
          isEmpty => {
            if (isEmpty) {
              this.isDuplicateLogin = true;
              this.userService.update(this.user)
                .subscribe(
                  user => {
                    this.user = user;
                    this.goToUserPage();
                  },
                  error => {
                    this.errorMessage = <any> error;
                  });
            }else {
              this.isDuplicateLogin = false;
            }
          },
          error => {
            this.errorMessage = <any> error;
          });
    }
  }

  goToUserPage() {
    this.router.navigate(['/admin/user/', this._user.id]);
  }

  returnToBack() {
    window.history.back();
  }


  get errorMessage(): string {
    return this._errorMessage;
  }

  set errorMessage(value: string) {
    this._errorMessage = value;
  }

  get user(): any {
    return this._user;
  }

  set user(value: any) {
    this._user = value;
  }

  get login(): string {
    return this._login;
  }

  set login(value: string) {
    this._login = value;
  }

  get rGuard(): RoleGuard {
    return this._rGuard;
  }

  set rGuard(value: RoleGuard) {
    this._rGuard = value;
  }

  get returnUrl(): string {
    return this._returnUrl;
  }

  set returnUrl(value: string) {
    this._returnUrl = value;
  }

  get isDuplicateLogin(): boolean {
    return this._isDuplicateLogin;
  }

  set isDuplicateLogin(value: boolean) {
    this._isDuplicateLogin = value;
  }

  get types(): (string | string)[] {
    return this._types;
  }

  set types(value: (string | string)[]) {
    this._types = value;
  }
}
