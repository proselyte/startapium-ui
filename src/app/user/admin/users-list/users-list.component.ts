import {Component, OnInit} from '@angular/core';
import {User} from "../../user";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../../user.service";
import {RoleGuard} from "../../../guards/role.guards";

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html'
})
export class UsersListComponent implements OnInit {
  private _errorMessage: string;
  private _usersMap: Map<string, User[]>;
  private _users: User[];
  private _specialists: User[];
  private _customers: User[];
  private _returnUrl: string;
  private _rGuard: RoleGuard;

  constructor(private roleGuard: RoleGuard,
              private route: ActivatedRoute,
              private router: Router,
              private userService: UserService) {
    this._rGuard = roleGuard;
  }

  ngOnInit() {
    this._returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

    this._usersMap = new Map<string, User[]>()
    this.userService.getAll().subscribe(
      usersMap => {
        Object.keys(usersMap).forEach(key => {
          if (key === 'users') {
            this._users = <User[]>usersMap[key];
          }
          if (key === 'specialists') {
            this._specialists = <User[]>usersMap[key];

          }
          if (key === 'customers') {
            this._customers = <User[]>usersMap[key];
          }
        })
      },
      error => this._errorMessage = <any> error);

  }

  onSelect(user: User) {
    this.router.navigate(['/admin/user/', user.id]);
  }

  add() {
    this.router.navigate(['/admin/user/add']);
  }

  delete(user: User) {
    this.router.navigate(['/admin/user/delete/', user.id]);
  }

  edit(user: User) {
    this.router.navigate(['/admin/user/edit/', user.id]);
  }


  get errorMessage(): string {
    return this._errorMessage;
  }

  set errorMessage(value: string) {
    this._errorMessage = value;
  }

  get usersMap(): Map<string, User[]> {
    return this._usersMap;
  }

  set usersMap(value: Map<string, User[]>) {
    this._usersMap = value;
  }

  get users(): User[] {
    return this._users;
  }

  set users(value: User[]) {
    this._users = value;
  }

  get specialists(): User[] {
    return this._specialists;
  }

  set specialists(value: User[]) {
    this._specialists = value;
  }

  get customers(): User[] {
    return this._customers;
  }

  set customers(value: User[]) {
    this._customers = value;
  }

  get returnUrl(): string {
    return this._returnUrl;
  }

  set returnUrl(value: string) {
    this._returnUrl = value;
  }

  get rGuard(): RoleGuard {
    return this._rGuard;
  }

  set rGuard(value: RoleGuard) {
    this._rGuard = value;
  }
}
