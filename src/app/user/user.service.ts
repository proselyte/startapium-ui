import {Injectable} from "@angular/core";
import {Http, Response, Headers, RequestOptions} from "@angular/http";
import {Observable} from "rxjs";
import {User} from "./user";
import {environment} from "../../environments/environment";
import {HttpResponse} from "selenium-webdriver/http";


@Injectable()
export class UserService {

  private entity_url = environment.REST_API_URL;

  constructor(private _http: Http) {
  }

  getAll(): Observable<Map<string, User[]>> {
    let token = localStorage.getItem('token');
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json;charset=UTF-8');
    headers.append('Authorization', `${token}`);
    return this._http.get(this.entity_url + 'app/user/all', {headers})
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  getById(id: string): Observable<User> {
    let token = localStorage.getItem('token');
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json;charset=UTF-8');
    headers.append('Authorization', `${token}`);
    return this._http.get(this.entity_url + 'app/user/' + id, {headers})
      .map((response: Response) => <User> response.json())
      .catch(this.handleError);
  }

  getByName(name: string): Observable<User> {
    let token = localStorage.getItem('token');
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json;charset=UTF-8');
    headers.append('Authorization', `${token}`);
    return this._http.get(this.entity_url + 'app/user/name/' + name, {headers})
      .map((response: Response) => <User> response.json())
      .catch(this.handleError);
  }

  isEmptyName(name: string): Observable<boolean> {
    let token = localStorage.getItem('token');
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json;charset=UTF-8');
    headers.append('Authorization', `${token}`);
    return this._http.get(this.entity_url + 'app/user/empty/' + name, {headers})
      .map((response: Response) => {
          if (response.status === 204) {
            return true;
          } else {
            return false;
          }
        }
      )
      .catch(this.handleError);
  }

  signUp(user: User): Observable<User> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json;charset=UTF-8');
    return this._http.post(this.entity_url + 'app/auth/signUp', JSON.stringify(user), {headers})
      .map((response: Response) => response.status)
      .catch(this.handleError);
  }

  save(user: User): Observable<User> {
    let token = localStorage.getItem('token');
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json;charset=UTF-8');
    headers.append('Authorization', `${token}`);
    return this._http.post(this.entity_url + 'app/user/add', JSON.stringify(user), {headers})
      .map((response: Response) => <User> response.json())
      .catch(this.handleError);
  }

  update(user: User): Observable<User> {
    let token = localStorage.getItem('token');
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json;charset=UTF-8');
    headers.append('Authorization', `${token}`);
    return this._http.put(this.entity_url + 'app/user/edit', JSON.stringify(user), {headers})
      .map((response: Response) => <User> response.json())
      .catch(this.handleError);
  }

  delete(id: string): Observable<User> {
    let token = localStorage.getItem('token');
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json;charset=UTF-8');
    headers.append('Authorization', `${token}`);
    return this._http.delete(this.entity_url + 'app/user/' + id, {headers})
      .map((response: Response) => response.status)
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    console.log('handleError log: ');
    let errMsg: string;
    if (error instanceof Response) {
      if (!(error.text() === '' )) {
        const body = error.json() || '';
        const err = body.error || JSON.stringify(body);
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      } else {
        console.log('binding errors header not empty');
        errMsg = error.headers.get('errors').toString();
      }
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
