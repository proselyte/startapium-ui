import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {Role} from '../role';
import 'rxjs/Rx';
import {RoleService} from "../role.service";
import {User} from "../../user/user";

@Component({
  selector: 'app-role-detail',
  templateUrl: './role-detail.component.html',
  styleUrls:['./role-detail.component.css']
})

export class RoleDetailComponent implements OnInit {

  errorMessage: string;
  role: Role;
  user: User;

  constructor(private route: ActivatedRoute, private router: Router, private roleService: RoleService) {
    this.role = <Role>{};
  }

  ngOnInit() {
    const id = this.route.snapshot.params['id'];
    this.roleService.getById(id).subscribe(
      role => this.role = role,
      error => this.errorMessage = <any> error);
  }

  rolesList() {
    this.router.navigate(['admin/role/']);
  }

  editRole() {
    this.router.navigate(['admin/role/edit/', this.role.id]);
  }

  delete(role: Role) {
    this.roleService.delete(role.id.toString()).subscribe(
      role => {
        this.role = role;
      },
      error => {
        this.errorMessage = <any> error;
      });
    this.rolesList();
  }

  editUser(user: User) {
    this.router.navigate(['/admin/user/edit/', user.id]);
  }

  onSelect(role: Role) {
    this.router.navigate(['/admin/user/', role.id]);
  }
}
