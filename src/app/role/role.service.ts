
import {Injectable} from "@angular/core";
import {Http, Response, Headers} from "@angular/http";
import {Observable} from "rxjs";
import {Role} from "./role";
import {environment} from "../../environments/environment";
import {User} from "../user/user";


@Injectable()
export class RoleService {

  private entity_url = environment.REST_API_URL;

  constructor(private _http : Http){
  }

  getAll(): Observable<Role[]> {
    let token = localStorage.getItem('token');
    const headers = new Headers();
    headers.append('Authorization', `${token}`);
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json;charset=UTF-8');
    return this._http.get(this.entity_url + '/admin/role/all', {headers})
      .map((response: Response) => <Role[]>response.json())
      .catch(this.handleError);
  }

  getById(role_id: string): Observable<Role> {
    let token = localStorage.getItem('token');
    const headers = new Headers();
    headers.append('Authorization', `${token}`);
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json;charset=UTF-8');
    return this._http.get(this.entity_url + 'admin/role/' + role_id, {headers})
      .map((response: Response) => <Role> response.json())
      .catch(this.handleError);
  }

  save(role: Role): Observable<Role> {
    let token = localStorage.getItem('token');
    const headers = new Headers();
    headers.append('Authorization', `${token}`);
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json;charset=UTF-8');
    return this._http.post(this.entity_url + 'admin/role/add', JSON.stringify(role), {headers})
      .map((response: Response) => response.status)
      .catch(this.handleError);
  }

  delete(id: string): Observable<Role> {
    let token = localStorage.getItem('token');
    const headers = new Headers();
    headers.append('Authorization', `${token}`);
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json;charset=UTF-8');
    return this._http.delete(this.entity_url + 'admin/role/' + id, {headers})
      .map((response: Response) => response.status)
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    console.log('handleError log: ');
    let errMsg: string;
    if (error instanceof Response) {
      if (!(error.text() === '' )) {
        const body = error.json() || '';
        const err = body.error || JSON.stringify(body);
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      } else {
        console.log('binding errors header not empty');
        errMsg = error.headers.get('errors').toString();
      }
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
