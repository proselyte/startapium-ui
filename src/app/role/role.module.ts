
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";

import {NgModule} from "@angular/core";
import {RoleRoutingModule} from "./role-routing.module";
import {RoleService} from "./role.service";
import {RoleListComponent} from "./role-list/role-list.component";
import {RoleDetailComponent} from "./role-detail/role-detail.component";
import {RoleEditComponent} from "./role-edit/role-edit.component";

@NgModule({

  imports: [
    CommonModule,
    FormsModule,
    RoleRoutingModule
  ],
  declarations: [
    RoleListComponent,
    RoleDetailComponent,
    RoleEditComponent,
  ],
  providers : [RoleService]
})

export class RoleModule {

}
