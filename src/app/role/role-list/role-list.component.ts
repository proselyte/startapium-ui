
import {Component, OnInit} from '@angular/core';
import 'rxjs/Rx';
import {Router} from '@angular/router';
import {Role} from "../role";
import {RoleService} from "../role.service";

@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.css']
})
export class RoleListComponent implements OnInit {

  errorMessage: string;
  roles: Role[];
  role:Role;

  constructor(private roleService: RoleService, private router : Router){
    this.role = <Role>{};
  }

  ngOnInit() {
    this.roleService.getAll().subscribe(
      roles => this.roles = roles,
      error => this.errorMessage = <any> error);
  }

  onSubmit(role:Role){
    role.id = null;
    this.roleService.save(role).subscribe(
      new_role => {
        this.role = new_role;
        this.ngOnInit();
      },
      error => this.errorMessage = <any>error
    );
  }

  onSelect(role: Role) {
    this.router.navigate(['/admin/role', role.id]);
  }

  edit(role: Role) {
    this.router.navigate(['admin/role/edit/', role.id]);
  }

  delete(role: Role) {
    this.roleService.delete(role.id.toString()).subscribe(
        role => {
          this.role = role;
          this.ngOnInit();
        },
        error => {
          this.errorMessage = <any> error;
        });
    this.ngOnInit();
  }


}
