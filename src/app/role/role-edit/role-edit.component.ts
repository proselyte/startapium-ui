
import {Component, OnInit} from '@angular/core';
import {RoleService} from '../role.service';
import {ActivatedRoute, Router} from '@angular/router';
import 'rxjs/Rx';
import {Role} from "../role";

@Component({
  selector: 'app-role-edit',
  templateUrl: './role-edit.component.html',
  styleUrls: ['./role-edit.component.css']
})
export class RoleEditComponent implements OnInit {
  public role;
  errorMessage: string;

  constructor(private roleService: RoleService, private route: ActivatedRoute, private router: Router) {
    this.role = <Role>{};
  }

  ngOnInit() {
    const id = this.route.snapshot.params['id'];
    this.roleService.getById(id).subscribe(
      role => this.role = role,
      error => this.errorMessage = <any> error);
  }

  onSubmit(role) {
    var that = this;
    this.roleService.save(role).subscribe(
      get_result,
      get_error
    );

    function get_error(error) {
      console.log(error);
      console.log('error caught');
      return this.errorMessage = <any> error;
    }

    function get_result(update_status) {
      console.log(update_status);
      if (update_status.status === 204) {
        console.log('update success');
        that.roleDetail(role);
      } else {
        return console.log('update failed');
      }
    }
    this.returnToBack();
  };


  roleDetail(role: Role) {
    this.router.navigate(['/admin/role/', role.id]);
  }

  returnToBack(){    window.history.back();  }


}
