import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {RoleListComponent} from "./role-list/role-list.component";
import {RoleDetailComponent} from "./role-detail/role-detail.component";
import {RoleEditComponent} from "./role-edit/role-edit.component";
import {AdminInputComponent} from "../admin-input/admin-input/admin-input.component";

const roleRoutes: Routes = [
  {
    path: 'admin',
    component: AdminInputComponent,
    children: [ {path: 'role', component: RoleListComponent},
                {path: 'role/:id', component: RoleDetailComponent},
                {path: 'role/edit/:id', component: RoleEditComponent}
              ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(roleRoutes)],
  exports: [RouterModule]
})

export class RoleRoutingModule {
}
