import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AppInputComponent} from "./app-input/app-input.component";
import {WelcomeComponent} from "../parts/welcome/welcome.component";
import {LoginComponent} from "../login/login/login.component";
import {RegisterComponent} from "../register/register/register.component";
import {PageNotFoundComponent} from "../parts/page-not-found/page-not-found.component";


const userRoutes: Routes = [

  {
    path: 'app',
    component: AppInputComponent,
    children: [
      {path: 'welcome', component: WelcomeComponent},
      {path: 'login', component: LoginComponent},
      {path: 'register', component: RegisterComponent},
      {path: '', component: WelcomeComponent, pathMatch: 'full'},
      {path: 'bad', component: PageNotFoundComponent}
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(userRoutes)],
  exports: [RouterModule]
})

export class AppInputRoutingModule {
}
