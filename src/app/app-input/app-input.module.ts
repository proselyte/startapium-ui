import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AppInputComponent} from './app-input/app-input.component';
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {LayoutModule} from "../layuot/layout.module";
import {PartsModule} from "../parts/parts.module";
import {LoginModule} from "../login/login.module";
import {RegisterModule} from "../register/register.module";
import {AuthGuard} from "../guards/auth.guard";
import {RoleGuard} from "../guards/role.guards";
import {AppInputRoutingModule} from "./app-input-routing.module";

@NgModule({
  imports: [
    FormsModule,
    HttpModule,
    CommonModule,
    LayoutModule,
    PartsModule,
    LoginModule,
    RegisterModule,
    AppInputRoutingModule
  ],
  providers: [AuthGuard, RoleGuard],
  declarations: [AppInputComponent]
})
export class AppInputModule {
}
