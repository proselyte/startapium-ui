import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {User} from "../user/user";

@Injectable()
export class RoleGuard implements CanActivate {

  private _isAdmin: boolean = false;

  constructor(private router: Router) {
    if (localStorage.getItem('user')) {
      const user: User = JSON.parse(localStorage.getItem('user'));
      user.roles.forEach(roleUser => {
        if (roleUser.name === 'ROLE_ADMIN') {
          this.isAdmin = true;
        }
      });
    }

  }


  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    if(this._isAdmin){
      return true;
    }else {
      this.router.navigate(['/ops'], {queryParams: {returnUrl: state.url}});
      return false;
    }

  }

  get isAdmin(): boolean {
    return this._isAdmin;
  }

  set isAdmin(value: boolean) {
    this._isAdmin = value;
  }
}
