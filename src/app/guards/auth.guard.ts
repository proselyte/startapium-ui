import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {User} from "../user/user";

@Injectable()
export class AuthGuard implements CanActivate {

  private _isAuthenticated: boolean = false;
  private _userName: string;

  constructor(private router: Router) {
    if (localStorage.getItem('token')) {
      this.isAuthenticated = true;
      const user: User = JSON.parse(localStorage.getItem('user'));
      this.userName = user.name;
    }
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (localStorage.getItem('token')) {
      return true;
    }else{
      this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});
      return false;
    }

  }

  get isAuthenticated(): boolean {
    return this._isAuthenticated;
  }

  set isAuthenticated(value: boolean) {
    this._isAuthenticated = value;
  }

  get userName(): string {
    return this._userName;
  }

  set userName(value: string) {
    this._userName = value;
  }

}
