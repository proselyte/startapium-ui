import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {UserBlockComponent} from "./user-block/user-block.component";
import {AuthGuard} from "../guards/auth.guard";
import {AuthenticationService} from "../login/authentication.service";
import {CollapseModule} from "ng2-bootstrap";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CollapseModule
  ],
  declarations: [
    UserBlockComponent
  ],
  exports: [
    UserBlockComponent
  ],
  providers:[AuthGuard,AuthenticationService]


})
export class UserBlockModule {
}
