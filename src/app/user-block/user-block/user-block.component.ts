import {Component, OnInit} from "@angular/core";
import {AuthGuard} from "../../guards/auth.guard";
import {AuthenticationService} from "../../login/authentication.service";

@Component({
  selector: 'app-user-block',
  templateUrl: './user-block.component.html'

})
export class UserBlockComponent implements OnInit {
  aGuard: AuthGuard;
  showMenuField: boolean = false;

  constructor(private authGuard: AuthGuard,
              private authenticationService: AuthenticationService) {
    this.aGuard = authGuard;
  }

  ngOnInit() {
  }

  logout() {
    this.authenticationService.logout()
  }

}
