import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminInputComponent} from './admin-input/admin-input.component';
import {RoleGuard} from "../guards/role.guards";
import {AuthGuard} from "../guards/auth.guard";
import {RegisterModule} from "../register/register.module";
import {LoginModule} from "../login/login.module";
import {PartsModule} from "../parts/parts.module";
import {LayoutModule} from "../layuot/layout.module";
import {HttpModule} from "@angular/http";
import {FormsModule} from "@angular/forms";
import {AdminInputRoutingModule} from "./admin-input-routing.module";


@NgModule({
  imports: [
    FormsModule,
    HttpModule,
    CommonModule,
    LayoutModule,
    PartsModule,
    LoginModule,
    RegisterModule,
    AdminInputRoutingModule
  ],
  providers: [AuthGuard, RoleGuard],
  declarations: [AdminInputComponent]
})
export class AdminInputModule {
}
