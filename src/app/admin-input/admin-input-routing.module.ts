import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WelcomeComponent} from "../parts/welcome/welcome.component";
import {AdminInputComponent} from "./admin-input/admin-input.component";
import {RoleGuard} from "../guards/role.guards";


const adminRoutes: Routes = [

  {
    path: 'admin',
    component: AdminInputComponent,
    children: [
      {path: '', component: WelcomeComponent},
      {path: '**', component: WelcomeComponent}
    ],
    canActivate: [RoleGuard]
  }


];

@NgModule({
  imports: [RouterModule.forChild(adminRoutes)],
  exports: [RouterModule]
})

export class AdminInputRoutingModule {
}
