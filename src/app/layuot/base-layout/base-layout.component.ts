import {Component, NgZone, OnInit} from "@angular/core";
import {AuthGuard} from "../../guards/auth.guard";
import {ActivatedRoute, Router} from "@angular/router";
import {RoleGuard} from "../../guards/role.guards";

@Component({
  selector: 'app-layout',
  template: '',
  styles: ['']
})


export class BaseLayoutComponent implements OnInit  {

  private _zoneT: NgZone;
  protected _aGuard: AuthGuard;
  protected _rGuard: RoleGuard;
  private static _left: string = '0';
  protected _routerT: Router;
  protected _returnUrl: string;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private authGuard : AuthGuard,
              private roleGuard : RoleGuard,
              private zone: NgZone) {
    this._routerT = router;
    this._aGuard = authGuard;
    this._rGuard = roleGuard;
    this._zoneT = zone;
    const mql1: MediaQueryList = window.matchMedia('(min-width: 768px)');

    mql1.addListener((mql: MediaQueryList) => {
      this._zoneT.run( () => {
        BaseLayoutComponent._left = mql.matches ? '0' : BaseLayoutComponent._left;
      });
    });
    const mql2: MediaQueryList = window.matchMedia('(max-width: 768px)');

    mql2.addListener((mql: MediaQueryList) => {
      this._zoneT.run( () => {
        BaseLayoutComponent._left = mql.matches ? '-250px' : BaseLayoutComponent._left;
      });
    });
  }

  ngOnInit(): void {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  toMain() {
    this.router.navigate(['/app']);
  }

  enterToLogin() {
    this.router.navigate(['/app/login']);
  }


  static get left(): string {
    if(window.innerWidth > 768){
      return '0';
    }else{
      return this._left;
    }
  }

  static set left(value: string) {
    this._left = value;
  }


  get returnUrl(): string {
    return this._returnUrl;
  }

  set returnUrl(value: string) {
    this._returnUrl = value;
  }
}
