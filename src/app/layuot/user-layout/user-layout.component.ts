import {Component, Injectable, NgZone, OnInit} from '@angular/core';
import {AuthGuard} from "../../guards/auth.guard";
import {TopBarComponent} from "../top-bar/top-bar.component";
import {ActivatedRoute, Router} from "@angular/router";
import {RoleGuard} from "../../guards/role.guards";
import {BaseLayoutComponent} from "../base-layout/base-layout.component";

@Component({
  selector: 'layout-user',
  templateUrl: './user-layout.component.html',
  styleUrls: ['./user-layout.component.css']
})

@Injectable()
export class UserLayoutComponent extends BaseLayoutComponent {

  private _leftU: string;

  constructor(route: ActivatedRoute,
              router: Router,
              authGuard : AuthGuard,
              roleGuard : RoleGuard,
              zone: NgZone){
    super(route,router,authGuard,roleGuard,zone);
    TopBarComponent.showBtnAddTeam = true;
  }


  ngOnInit(): void{
    super.ngOnInit();
    TopBarComponent.showBtnAddTeam = true;
  }

  toAdmin() {
    this._routerT.navigate(['/admin']);
  }


  get aGuard(): AuthGuard {
    return this._aGuard;
  }

  set aGuard(value: AuthGuard) {
    this._aGuard = value;
  }

  get rGuard(): RoleGuard {
    return this._rGuard;
  }

  set rGuard(value: RoleGuard) {
    this._rGuard = value;
  }


  get leftU(): string {
    return BaseLayoutComponent.left;
  }

}

