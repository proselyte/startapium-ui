import {Component, Injectable, NgZone} from '@angular/core';
import {TopBarComponent} from "../top-bar/top-bar.component";
import {AuthGuard} from "../../guards/auth.guard";
import {ActivatedRoute, Router} from "@angular/router";
import {RoleGuard} from "../../guards/role.guards";
import {BaseLayoutComponent} from "../base-layout/base-layout.component";

@Component({
  selector: 'layout-admin',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.css']
})


@Injectable()
export class AdminLayoutComponent extends BaseLayoutComponent{

  private _leftA: string;

  constructor(route: ActivatedRoute,
              router: Router,
              authGuard : AuthGuard,
              roleGuard : RoleGuard,
              zone: NgZone){
    super(route,router,authGuard,roleGuard,zone);
    TopBarComponent.showBtnAddTeam = false;
  }


  ngOnInit(): void{
    super.ngOnInit();
    TopBarComponent.showBtnAddTeam = false;
  }

  toUser() {
    this._routerT.navigate(['/app']);
  }

  usersAll() {
    this._routerT.navigate(['/admin/user/all']);
  }

  rolesAll(){
    this._routerT.navigate(['/admin/role']);
  }

  get aGuard(): AuthGuard {
    return this._aGuard;
  }

  set aGuard(value: AuthGuard) {
    this._aGuard = value;
  }

  get rGuard(): RoleGuard {
    return this._rGuard;
  }

  set rGuard(value: RoleGuard) {
    this._rGuard = value;
  }

  get leftA(): string {
    return BaseLayoutComponent.left;
  }

  set leftA(value: string) {
    this._leftA = value;
  }
}
