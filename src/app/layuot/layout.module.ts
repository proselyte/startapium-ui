import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {BaseLayoutComponent} from "./base-layout/base-layout.component";
import {UserBlockModule} from "../user-block/user-block.module";
import {AuthGuard} from "../guards/auth.guard";
import {UserLayoutComponent} from './user-layout/user-layout.component';
import {AdminLayoutComponent} from './admin-layout/admin-layout.component';
import {TopBarComponent} from './top-bar/top-bar.component';
import {RoleGuard} from "../guards/role.guards";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    UserBlockModule
  ],
  declarations: [
    BaseLayoutComponent,
    UserLayoutComponent,
    AdminLayoutComponent,
    TopBarComponent
  ],
  exports: [
    BaseLayoutComponent,
    AdminLayoutComponent,
    UserLayoutComponent,
    TopBarComponent
  ],
  providers: [
    AuthGuard,
    RoleGuard,
    TopBarComponent,
    UserLayoutComponent,
    BaseLayoutComponent,
    AdminLayoutComponent
  ]
})
export class LayoutModule {

}
