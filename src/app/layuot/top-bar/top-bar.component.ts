import {Component, Injectable, OnInit} from '@angular/core';
import {AuthGuard} from "../../guards/auth.guard";
import {BaseLayoutComponent} from "../base-layout/base-layout.component";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css']
})

@Injectable()
export class TopBarComponent implements OnInit {

  static _showBtnAddTeam;
  aGuard : AuthGuard;
  baseComponent: BaseLayoutComponent;
  private _showAddTeam : boolean;
  private _returnUrl: string;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private authGuard : AuthGuard,
              private baseLayoutComponent : BaseLayoutComponent) {
    this.aGuard = authGuard;
    this.baseComponent=baseLayoutComponent;
    TopBarComponent._showBtnAddTeam = true;
  }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  hamburgerClick(){
    if(BaseLayoutComponent.left === '-250px'){
      BaseLayoutComponent.left = '0';
    }else{
      BaseLayoutComponent.left = '-250px';
    }
  }

  toMain() {
    this.router.navigate(['/app']);
  }

  static get showBtnAddTeam() {
    return TopBarComponent._showBtnAddTeam;
  }

  static set showBtnAddTeam(value) {
    TopBarComponent._showBtnAddTeam = value;
  }

  get showAddTeam(): boolean {
    return TopBarComponent._showBtnAddTeam;
  }

  set showAddTeam(value: boolean) {
    this._showAddTeam = value;
  }

  get returnUrl(): string {
    return this._returnUrl;
  }

  set returnUrl(value: string) {
    this._returnUrl = value;
  }
}
