import { StartapiumFrontPage } from './app.po';

describe('startapium-front App', () => {
  let page: StartapiumFrontPage;

  beforeEach(() => {
    page = new StartapiumFrontPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
